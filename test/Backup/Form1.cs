﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {


        StreamWriter f = new StreamWriter("Buf.dll");

        int counter = 0;
        
        int  prot=0;
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Text = "Введите Ваше Ф.И.О. :";
            label2.Hide();
            radioButton1.Hide();
            radioButton2.Hide();
            radioButton3.Hide();
            radioButton4.Hide();
            button1.Hide();
            
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
             StreamWriter fv = new StreamWriter(textBox1.Text+".txt");
                f = fv;
             
            f.WriteLine("Тестируемый(-ая): " + textBox1.Text);
            f.WriteLine("\nВопрос № 1");
            if (radioButton2.Checked) 
            {
                f.WriteLine("Решите пример:\n2х - 4 = 2 чему равен х ?\nx = 3\tВерно !"); prot+=1;
                // x = 3
            }
              
            if (radioButton1.Checked) f.WriteLine("Решите пример:\n2х - 4 = 2 чему равен х ?\nx = 2\tНеверно !"); 
            if (radioButton3.Checked) f.WriteLine("Решите пример:\n2х - 4 = 2 чему равен х ?\nx = 4\tНеверно !"); 
            if (radioButton4.Checked) f.WriteLine("Решите пример:\n2х - 4 = 2 чему равен х ?\nx = 2,5\tНеверно !"); 
            label2.Text = "Решите пример:\n2 + 2 * 2 = ?"; // = 6
            radioButton1.Text = " 8 ";
            radioButton2.Text = " 4 ";
            radioButton3.Text = " 6 ";
            radioButton4.Text = " 9 ";
            button1.Hide();
            button3.Show();
            label1.Text = "Вопрос № 2";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            label5.Visible = true;
            label6.Visible = true;
            label7.Visible = false;

            

            label1.Text = "Вопрос № 1";
            label2.Show();
            radioButton1.Show();
            radioButton2.Show();
            radioButton3.Show();
            radioButton4.Show();
            button1.Show();
            button2.Hide();
            textBox1.Hide();
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            f.WriteLine("\nВопрос № 2");
            if (radioButton1.Checked) f.WriteLine("Решите пример:\n2 + 2 * 2 = 8\tНеверно!");
            if (radioButton2.Checked) f.WriteLine("Решите пример:\n2 + 2 * 2 = 4\tНеверно!");
            if (radioButton3.Checked)
            {
                f.WriteLine("Решите пример:\n2 + 2 * 2 = 6\tВерно!"); prot+=1;
            }
            
            if (radioButton4.Checked) f.WriteLine("Решите пример:\n2 + 2 * 2 = 9\tНеверно!"); 
            label1.Text = "Вопрос № 3";
            label2.Text = "Вычислите квадратный корень из 1024";
            radioButton1.Text = " 32 "; 
            radioButton2.Text = " 16 ";
            radioButton3.Text = " 24 ";
            radioButton4.Text = " 48 ";
            button3.Hide();
            button4.Show();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            f.WriteLine("\nВопрос № 3");
            if (radioButton1.Checked)
            {
                f.WriteLine("Вычислите квадратный корень из 1024 = 32\tВерно!"); prot+=1;
            }
          
            if (radioButton2.Checked) f.WriteLine("Вычислите квадратный корень из 1024 = 16\tНеверно!"); 
            if (radioButton3.Checked) f.WriteLine("Вычислите квадратный корень из 1024 = 24\tНеверно!"); 
            if (radioButton4.Checked) f.WriteLine("Вычислите квадратный корень из 1024 = 48\tНеверно!");
            label1.Text = "Вопрос № 4";
            label2.Text = "Если X*Z=Y, то чему равен Z ?"; // Z=Y/X
            radioButton1.Text = "Z=X*Y";
            radioButton2.Text = "Z=X/Y";
            radioButton3.Text = "Z=Y/X";
            radioButton4.Text = "Z=Y*X";
            button4.Hide();
            button5.Show();
        
        }

        private void button5_Click(object sender, EventArgs e)
        {
            f.WriteLine("\nВопрос № 4");
            if (radioButton1.Checked) f.WriteLine("Если X*Z=Y, то чему равен Z=X*Y\tНеверно!"); 
            if (radioButton2.Checked) f.WriteLine("Если X*Z=Y, то чему равен Z=X/Y\tНеверно!");
            if (radioButton3.Checked)
            {
                f.WriteLine("Если X*Z=Y, то чему равен Z=Y/X\tВерно!"); prot+=1;
            }
           
            if (radioButton4.Checked) f.WriteLine("Если X*Z=Y, то чему равен Z=Y*X\tНеверно!"); 
            label1.Text = "Вопрос № 5";
            label2.Text = "Решите пример:\n(128/16+3)+2,5*11 = ?";// 38,5
            radioButton1.Text = "28,5";
            radioButton2.Text = "48,5";
            radioButton3.Text = "11,5";
            radioButton4.Text = "38,5";
            button5.Hide();
            button6.Show();

        }

        private void button6_Click(object sender, EventArgs e)
        {
            f.WriteLine("\nВопрос № 5");
            if (radioButton1.Checked) f.WriteLine("Решите пример:\n(128/16+3)+2,5*11 = 28,5\tНеверно!"); 
            if (radioButton2.Checked) f.WriteLine("Решите пример:\n(128/16+3)+2,5*11 = 48,5\tНеверно!"); 
            if (radioButton3.Checked) f.WriteLine("Решите пример:\n(128/16+3)+2,5*11 = 11,5\tНеверно!");
            if (radioButton4.Checked)
            {
                f.WriteLine("Решите пример:\n(128/16+3)+2,5*11 = 38,5\tВерно!"); prot+=1;
            }
          
            label1.Text = "Вопрос № 6";
            label2.Text = "15 в третьей степени:";//3375
            radioButton1.Text = "2345";
            radioButton2.Text = "3375";
            radioButton3.Text = "2114";
            radioButton4.Text = "3774";
            button6.Hide();
            button7.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            f.WriteLine("\nВопрос № 6");
            if (radioButton1.Checked) f.WriteLine("15 в третьей степени = 2345\tНеверно!");
            if (radioButton2.Checked)
            {
                f.WriteLine("15 в третьей степени = 3375\tВерно!"); prot+=1;
            }
          
            if (radioButton3.Checked) f.WriteLine("15 в третьей степени = 2114\tНеверно!"); 
            if (radioButton4.Checked) f.WriteLine("15 в третьей степени = 3774\tНеверно!"); 
            label1.Text = "Вопрос № 7";
            label2.Text = "Десятичный логарифм из 100 = ?";//2
            radioButton1.Text = "3";
            radioButton2.Text = "4";
            radioButton3.Text = "1";
            radioButton4.Text = "2";
            button7.Hide();
            button8.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            f.WriteLine("\nВопрос № 7");
            if (radioButton1.Checked) f.WriteLine("Десятичный логарифм из 100 = 3\tНеверно!");
            if (radioButton2.Checked) f.WriteLine("Десятичный логарифм из 100 = 4\tНеверно!"); 
            if (radioButton3.Checked) f.WriteLine("Десятичный логарифм из 100 = 1\tНеверно!");
            if (radioButton4.Checked)
            {
                f.WriteLine("Десятичный логарифм из 100 = 2\tВерно!"); prot+=1;
            }
            
            label1.Text = "Вопрос № 8";
            label2.Text = "Приближенное значение массы Земли равно\n(5,98 +- 0,01) * 10^24 кг. Масса пули\nохотничьего ружья равна (9 +- 1) г.\nОцените относительную погрешность\nкаждого измерения ?";
            radioButton1.Text = "Земля ~ 0,2%\nПуля ~ 11%";//!
            radioButton2.Text = "Земля ~ 8%\nПуля ~ 0,9%";
            radioButton3.Text = "Земля ~ 5%\nПуля ~ 0,1%";
            radioButton4.Text = "Земля ~ 0,3%\nПуля ~ 8%";
            button8.Hide();
            button9.Show();
        
        }

        private void button9_Click(object sender, EventArgs e)
        {
            f.WriteLine("\nВопрос № 8");
            f.WriteLine("Приближенное значение массы Земли равно\n(5,98 +- 0,01) * 10^24 кг. Масса пули\nохотничьего ружья равна (9 +- 1) г.\nОцените относительную погрешность каждого измерения ?");
            if (radioButton1.Checked)
            {
                f.WriteLine("Земля ~ 0,2%  Пуля ~ 11%\tВерно!"); prot+=1;
            }
         
            if (radioButton2.Checked) f.WriteLine("Земля ~ 8%  Пуля ~ 0,9\tНеверно!"); 
            if (radioButton3.Checked) f.WriteLine("Земля ~ 5%  Пуля ~ 0,1%\tНеверно!"); 
            if (radioButton4.Checked) f.WriteLine("Земля ~ 0,3%  Пуля ~ 8%\tНеверно!"); 
            label1.Text = "Вопрос № 9";
            label2.Text = "Вычислить квадратный корень из\nвыражения  54 * 24";
            radioButton1.Text = "23";
            radioButton2.Text = "36"; //!
            radioButton3.Text = "26";
            radioButton4.Text = "38";
            button9.Hide();
            button10.Show();
        
        }

        private void button10_Click(object sender, EventArgs e)
        {
            f.WriteLine("\nВопрос № 9");
            if (radioButton1.Checked) f.WriteLine("Вычислить квадратный корень из\nвыражения  54 * 24 = 23\tНеверно!");
            if (radioButton2.Checked)
            {
                f.WriteLine("Вычислить квадратный корень из\nвыражения  54 * 24 = 36\tВерно!"); prot+=1;
            }
        
            if (radioButton3.Checked) f.WriteLine("Вычислить квадратный корень из\nвыражения  54 * 24 = 26\tНеверно!"); 
            if (radioButton4.Checked) f.WriteLine("Вычислить квадратный корень из\nвыражения  54 * 24 = 38\tНеверно!"); 
            label1.Text = "Вопрос № 10";
            label2.Text = "Решите систему уравнений:\n                                   { x + y = 3\n                                   { xy = -10";
            radioButton1.Text = "(3;-2),(-3;5)";
            radioButton2.Text = "(-2;4),(2;-5)";
            radioButton3.Text = "(-5;2),(5;-2)";
            radioButton4.Text = "(5;-2),(-2;5)"; //!
            button10.Hide();
            button12.Show();
        
        
        }

        

        private void button12_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            f.WriteLine("\nВопрос № 10");
            f.WriteLine("Решите систему уравнений:\nx + y = 3\nxy = -10");
            if (radioButton1.Checked) f.WriteLine("Ответ: (3;-2),(-3;5)\tНеверно!"); 
            if (radioButton2.Checked) f.WriteLine("Ответ: (-2;4),(2;-5)\tНеверно!"); 
            if (radioButton3.Checked) f.WriteLine("Ответ: (-5;2),(5;-2)\tНеверно!");
            if (radioButton4.Checked)
            {
                f.WriteLine("Ответ: (5;-2),(-2;5)\tВерно!"); prot+=1;
            }
        
            
            
            string ot = Convert.ToString(prot);
            label1.Text = "Правильных ответов: "+ ot;
            label2.Text = "Тестировался (-ась):\n" + textBox1.Text;
            radioButton1.Hide();
            radioButton2.Hide();
            radioButton3.Hide();
            radioButton4.Hide();
            button12.Hide();
            button13.Show();
            f.WriteLine("\nКоличество правильных ответов: " + ot);
            label4.Show();
            if (0 <= prot && prot <= 2) 
            {
                f.WriteLine("\n\tРезультат тестирования: Очень плохо, 1 (кол)"); label4.Text = "1 (Очень плохо)";
            }
            else if (3 <= prot && prot <= 4)
            {
                f.WriteLine("\n\tРезультат тестирования: Плохо, 2 (двойка)");label4.Text = "2 (Плохо)";
            }
            else if (5 <= prot && prot <= 6)
            {
                f.WriteLine("\n\tРезультат тестирования: Удовлетворительно, 3 (тройка)");label4.Text = "3 (Нормально)";
            }
            else if (7 <= prot && prot <= 8)
            {
                f.WriteLine("\n\tРезультат тестирования: Хорошо, 4 (четвёрка)"); label4.Text = "4 (Хорошо)";
            }
            else if (9 <= prot && prot <= 10)
            {
                f.WriteLine("\n\tРезультат тестирования: Отлично, 5 (пять)"); label4.Text = "5 (Отлично)";
            }
            label3.Show();
            f.Close();


            /* Все ответы правильные, либо один неправильный ответ 5 (Отлично);
             * Восемь или семь правильных ответа 4 (Хорошо);
             * Шесть или пять правильных ответов 3 (Нормально);
             * Четыре или три неправильных ответов 2 (Плохо);
             * Два, один правильных или ни одного правильного ответа 1 (Очень плохо);*/

        }

        private void button13_Click(object sender, EventArgs e)
        {
            Application.Exit();
            
        }

        private void InitializeTimer()
        {
          counter = 1;
          timer1.Enabled = false;
          this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
        }
        

        private void timer1_Tick(object sender, EventArgs e)
        {
            
            if (counter >= 900)
            {
                f.WriteLine("\n\tВремя вышло!");
                f.WriteLine("\nКоличество правильных ответов: " + prot);

                if (0 <= prot && prot <= 2)
                {
                    f.WriteLine("\n\tРезультат тестирования: Очень плохо, 1 (кол)"); label4.Text = "1 (Очень плохо)";
                }
                else if (3 <= prot && prot <= 4)
                {
                    f.WriteLine("\n\tРезультат тестирования: Плохо, 2 (двойка)"); label4.Text = "2 (Плохо)";
                }
                else if (5 <= prot && prot <= 6)
                {
                    f.WriteLine("\n\tРезультат тестирования: Удовлетворительно, 3 (тройка)"); label4.Text = "3 (Нормально)";
                }
                else if (7 <= prot && prot <= 8)
                {
                    f.WriteLine("\n\tРезультат тестирования: Хорошо, 4 (четвёрка)"); label4.Text = "4 (Хорошо)";
                }
                else if (9 <= prot && prot <= 10)
                {
                    f.WriteLine("\n\tРезультат тестирования: Отлично, 5 (пять)"); label4.Text = "5 (Отлично)";
                }
                f.Close(); Application.Exit();
            }
            else
            {
                counter = counter + 1;
                int min = counter / 60;
                label5.Text = min.ToString() + " мин.";
                

            }


            
            
            
            
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Программу создал:\nШаповалов А.А.\nКыргызстан, г.Бишкек");
        }

       

        

        

       
    }
}
