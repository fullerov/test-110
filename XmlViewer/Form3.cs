﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Tools;

namespace XmlViewer
{
    public partial class Form3 : Form
    {
        List<Tester> _testers;
        string[] _files;
        public Form3()
        {
            InitializeComponent();
        }
        public Form3(List<Tester> testers)
        {
            InitializeComponent();
            _testers = testers;
            GetFiles();
            progressBar1.Maximum = _files.Length;
            progressBar1.Step = 1;
            progressBar1.Value = 0;
            label1.Text = $"Обработано 0 из {_files.Length} файлов.";
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            ReadFiles();
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
            label1.Text = $"Обработано {e.ProgressPercentage} из {_files.Length} файлов.";
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Close();
        }

        private void GetFiles()
        {
            if (Directory.Exists("Результаты\\") == false)
            { System.IO.Directory.CreateDirectory("Результаты\\"); }
            _files = System.IO.Directory.GetFiles("Результаты\\", "*.res");
        }

        private void ReadFiles()
        {
            var crypt = new Crypto();

            int cnt = _files.Length;
            for (var i = 0; i < cnt; i++)
            {
                string res = "";
                var tester = new Tester();
                res += "Файл результатов: " + _files[i] + Environment.NewLine;
                var encryptedString = File.ReadAllText(_files[i]);
                var text = crypt.Decrypt(encryptedString);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(text);

                foreach (XmlElement node in doc.GetElementsByTagName("tests"))
                {
                    foreach (XmlElement x in node)
                    {
                        if (x.Name == "id")
                        {
                            res += "ID пользователя: " + x.InnerText + Environment.NewLine;
                            tester.Id = x.InnerText;
                        }
                        else if (x.Name == "date")
                        {
                            res += "Начало теста: " + x.InnerText + Environment.NewLine;
                            tester.DateStartString = x.InnerText;
                        }
                        else if (x.Name == "result")
                        {
                            var rate = 0;
                            res += "Всего баллов: " + x.InnerText + Environment.NewLine;
                            int.TryParse(x.InnerText, out rate);
                            tester.Rate = rate;
                        }
                        else if (x.Name == "info")
                        {
                            res += x.InnerText + Environment.NewLine;
                            tester.Info = x.InnerText;
                        }
                        else if (x.Name == "enddate")
                        {
                            res += "Дата завершения теста: " + x.InnerText + Environment.NewLine;
                            tester.DateEndString = x.InnerText;
                        }

                    }
                }
                res += Environment.NewLine + Environment.NewLine;
                res += "Детализация по вопросам: " + Environment.NewLine + Environment.NewLine;

                foreach (XmlElement node in doc.GetElementsByTagName("test"))
                {

                    foreach (XmlElement x in node)
                    {
                        if (x.Name == "number")
                        {
                            res += Environment.NewLine + "Вопрос №" + x.InnerText + Environment.NewLine +
                                   Environment.NewLine;
                        }
                        else if (x.Name == "vartext")
                        {
                            res += "Текст вопроса: " + x.InnerText + Environment.NewLine + Environment.NewLine;
                        }
                        else if (x.Name == "var")
                        {
                            if (x.InnerText.Length > 0)
                                res += "Ответ пользователя № " + x.InnerText + Environment.NewLine + Environment.NewLine;
                            else
                                res += "Ответ пользователя - пропуск вопроса (ответа нет)" + Environment.NewLine +
                                       Environment.NewLine;
                        }
                        else if (x.Name == "right")
                        {
                            if (x.InnerText == "true")
                                res += "Ответ правильный" + Environment.NewLine + Environment.NewLine;
                            else
                                res += "Ответ неправильный" + Environment.NewLine + Environment.NewLine;
                        }
                        else if (x.Name == "points")
                        {
                            res += "Текущее количество баллов: " + x.InnerText + Environment.NewLine +
                                   Environment.NewLine;
                        }
                    }

                }
                res +=
                    "____________________________________________________________________________________________________________________________" +
                    Environment.NewLine + Environment.NewLine;
                tester.Details = res;
                _testers.Add(tester);
                backgroundWorker1.ReportProgress(i + 1);
            }
        }
    }
}
