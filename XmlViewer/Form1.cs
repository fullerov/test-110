﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Tools;

namespace XmlViewer
{
    public partial class Form1 : Form
    {
        List<Tester> _testers;
        public Form1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FillTable()
        {
            dataGridView1.DataSource = _testers;
            dataGridView1.Columns[5].Visible = false;
            dataGridView1.Refresh();
        }

        private void ExportExcel(string fileName)
        {
            if (!_testers.Any())
            {
                MessageBox.Show("Нет данных для экспорта");
                return;
            }
            //StreamWriter wr = new StreamWriter(@"C:\XMLT2016\Results.csv");
            //var fileName = @"C:\XMLT2016\Results-" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";
            ExcelPackage pck = new ExcelPackage(new FileInfo(fileName));
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Results");
            var table = ws.Cells["A1"].LoadFromCollection(_testers.Select(a=> new {a.Id,a.Rate,a.Info,a.DateStartString,a.DateEndString}), true, TableStyles.Light1);
            pck.Save();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            string filename = saveFileDialog1.FileName+".xlsx";
            ExportExcel(filename);
            MessageBox.Show("Экспорт выполнен");
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var details = (string)dataGridView1.Rows[e.RowIndex].Cells[5].Value;
            var form2 = new Form2(details);
            form2.ShowDialog();
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            _testers = new List<Tester>();
            var pb = new Form3(_testers);
            pb.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            pb.ShowDialog();
            FillTable();
        }
    }

    public class Tester
    {
        public Tester()
        {
        }
        public string Id { get; set; }
        public string DateStartString { get; set; }
        public string DateEndString { get; set; }
        public int Rate { get; set; }
        public string Info { get; set; }
        public string Details { get; set; }
    }
}
